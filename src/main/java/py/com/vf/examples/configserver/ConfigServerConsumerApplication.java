package py.com.vf.examples.configserver;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RefreshScope
public class ConfigServerConsumerApplication {

	@Value("${user.role}") private String role;
	public static void main(String[] args) {
		SpringApplication.run(ConfigServerConsumerApplication.class, args);
	}
	
	@GetMapping("/user/{username}")
	public String prueba(@PathVariable("username") String username) {
		return "Hello: "+ role + " " + username;
	}
}
